var canvas = document.getElementById("system");
var centerX = canvas.width / 2;
var centerY = canvas.height / 2;
var ctx = canvas.getContext("2d");
draw_background(ctx);
draw_sun(ctx);

let planets = []
planets.push(new Planet(ctx, "Kerbin", "#00B1FD", 128, Math.PI, 10));
planets.push(new Planet(ctx, "Eve", "#0000FF", 64, 0.75*Math.PI, 5));
planets.push(new Planet(ctx, "Jool", "#FFAA22", 200, 0.125*Math.PI, 20));

let fleets = []
fleets.push(new Fleet(ctx, 0, 10, planets[0], planets[1], 0.5));
